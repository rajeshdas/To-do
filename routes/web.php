<?php

use App\Http\Controllers\TodolistController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//get todolist
Route::get('/', [TodolistController::class, 'index'])->name('task.index');

//store todolist
Route::post('/Add-new-task', [TodolistController::class, 'store'])->name('task.store');

//edit todolist
Route::get('/edit-task/{id}', [TodolistController::class, 'edit'])->name('edit.task');

// update todolist
Route::post('/update-task/{id}', [TodolistController::class, 'update'])->name('task.update');

//delete todolist
Route::get('/delete-task/{id}', [TodolistController::class, 'destroy'])->name('destroy.task');
