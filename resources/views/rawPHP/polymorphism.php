<?php
//superclass
abstract class Animal {
    abstract public function makeSound();
}

//subclasses
class Cow extends Animal {
    public function makeSound() {
        return "Hamba!";
    }
}

//subclasses
class Cat extends Animal {
    public function makeSound() {
        return "Meow!";
    }
}

// Example
function animalSounds($animal) {
    echo $animal->makeSound() . "\n";
}
$cow = new Cow();
$cat = new Cat();

animalSounds($cow);
animalSounds($cat); 

?>