<?php
//superclass
class Shape {
    public function calculateArea() {
        return 0;
    }
}

//subclasses
class Circle extends Shape {
    private $radius;

    public function __construct($radius) {
        $this->radius = $radius;
    }

    public function calculateArea() {
        return pi() * $this->radius * $this->radius;
    }
}

//subclasses
class Rectangle extends Shape {
    private $width;
    private $height;

    public function __construct($width, $height) {
        $this->width = $width;
        $this->height = $height;
    }

    public function calculateArea() {
        return $this->width * $this->height;
    }
}

// Example
$circle = new Circle(2);
$rectangle = new Rectangle(2, 3);

echo "Circle Area: " . $circle->calculateArea() ."\n";
echo "Rectangle Area: " . $rectangle->calculateArea();

?>