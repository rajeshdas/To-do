<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>To-Do-List</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
</head>
<body>
    <div class="container-fluid mt-5">
        <div class="row justify-content-center">
            <div class="col-md-4 mx-auto">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-center">To-Do List</h3>
                </div>
                <div class="card-body">
                    <ul class="list-group" id="todo-list">
                        <!-- List items will be added dynamically using JavaScript -->
                    </ul>
                </div>
                <div class="card-footer">
                    <form action="{{route('task.update',['id'=>$todo->id])}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                                @error('task')
                                    <small class="text-danger">{{$message}}</small>
                                @enderror
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="task" value="{{$todo->task}}">
                            </div>
                            <div class="col-md-4 d-flex">
                                <button type="submit" class="btn btn-primary btn-sm" > Update Task</button>
                                <a href="{{route('task.index')}}"><button type="button" class="btn btn-success">Back</button></a>
                            </div>
                        </div>
                        
                    </form>
                    <table class="table mt-5">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Task</td>
                                <td>Actions</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($todos as $todo)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$todo->task}}</td>
                                    <td>
                                        <a href="{{route('edit.task',['id'=>$todo->id])}}" class="btn btn-primary btn-sm">Edit</a>                                       
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"></script>
</body>
</html>