<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Todo;

class TodolistController extends Controller
{
    //list of todolist
    public function index()
    {
        $todos = Todo::orderBy('id','DESC')->get();
        return view('index', compact('todos'));
    }

    //store todolist
    public function store(Request $request)
    {
        $request->validate([
            'task' => 'required'
        ], 
        [
            'task.required' => 'Task field is required'
        ]);
        
        $todo = new Todo();
        $todo->task = $request->task;
        $todo->save();
        return redirect()->back()->with('success','Task Added Successfully');
    }

    //edit todolist
    public function edit($id)
    {
        $todos = Todo::all();
        $todo = Todo::findOrFail($id);
        return view('edit', compact('todo','todos'));
    }

    //update todolist
    public function update(Request $request,$id)
    {
        $request->validate([
            'task' => 'required'
        ], 
        [
            'task.required' => 'Task field is required'
        ]);
        
        $todo = Todo::findOrFail($id);
        $todo->task = $request->task;
        $todo->save();
        return redirect()->back()->with('success','Task Updated Successfully');
    }

    //delete todolist
    public function destroy($id)
    {
        $todo = Todo::findOrFail($id);
        $todo->delete();
        return redirect()->back()->with('success','Task Deleted Successfully');
    }
}
